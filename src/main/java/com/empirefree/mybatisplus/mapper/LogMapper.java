package com.empirefree.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.empirefree.mybatisplus.entity.LogDO;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/01/24 16:42
 */


public interface LogMapper extends BaseMapper<LogDO> {
}
