package com.empirefree.mybatisplus.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.jws.WebResult;
import javax.servlet.http.HttpServletResponse;

/**
 * @program: mybatisplus
 * @description:
 *
 * 学习来源：https://www.cnblogs.com/lgjlife/p/10988439.html
 * @ControllerAdvice和@ExceptionHandler：在DispatcherServlet返回给modelandView之前（即afterCompletion之前）
 * ErrorController 可以处理所有异常，包括不在控制器路径的错误，但是拦截器却将其看成2次请求，一次在SpringMvc的dispatcherServlet
 * 未进入Controller请求一次，接下来在进入Controller之后报错了又认为请求了一次。
 *
 * @author: huyuqiao
 * @create: 2022/02/08 14:36
 */
@Slf4j
@ControllerAdvice
@RestController
public class GloabalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public String globalException(HttpServletResponse response, Exception e){
        log.info("GloabalExceptionHandler.globalException--全局异常处理... {}", response.getStatus());
        return e.getMessage();
    }
}


