package com.empirefree.mybatisplus.annotation;

import java.lang.annotation.*;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/01/24 16:38
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoginLogPlus {
    /**
     * 编辑的表主键
     * @return
     */
    String editTableId() default "id";

    /**
     * 编辑的表名称
     * @return
     */
    String editTableName() default "未知";
}
