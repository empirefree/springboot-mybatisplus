package com.empirefree.mybatisplus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.empirefree.mybatisplus.entity.User;
import com.empirefree.mybatisplus.mapper.UserMapper;
import com.empirefree.mybatisplus.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Queue;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author empirefree
 * @since 2020-08-30
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    UserMapper userMapper;
    @Override
    public User selectOneUser(Integer id) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(User::getId, id);
        return userMapper.selectOne(queryWrapper);
    }

    @Override
    public User selectOneUserForUpdate(Integer id) {
        return userMapper.selectOneUserForUpdate(id);
    }

    @Override
    public Integer selectCount() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        return userMapper.selectCount(queryWrapper);
    }

    @Override
    public Integer insertUser() {
        User user = new User();
        user.setName("胡宇乔啊");
        return userMapper.insert(user);
    }
}
