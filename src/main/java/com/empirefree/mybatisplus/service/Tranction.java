package com.empirefree.mybatisplus.service;

import com.empirefree.mybatisplus.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2021/09/04 16:25
 */
@Service
@Slf4j
public class Tranction {
    @Autowired
    IUserService userService;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void insertOrRollback(){
        log.info("begin to insert");
        User user = new User();
        user.setId(8);
        user.setName("asdaaf");
        userService.saveOrUpdate(user);
        int a = 5/ 0;
        System.out.println("QAQ");
    }
}
