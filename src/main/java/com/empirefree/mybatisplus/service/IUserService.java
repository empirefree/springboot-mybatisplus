package com.empirefree.mybatisplus.service;

import com.empirefree.mybatisplus.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author empirefree
 * @since 2020-08-30
 */
public interface IUserService extends IService<User> {
    @Select("select * from user where id = #{id}}")
    User selectOneUser(@Param("id") Integer id);

    User selectOneUserForUpdate(@Param("id") Integer id);

    Integer selectCount();

    Integer insertUser();
}
