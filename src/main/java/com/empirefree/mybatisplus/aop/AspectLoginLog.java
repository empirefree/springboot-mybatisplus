package com.empirefree.mybatisplus.aop;

import com.alibaba.fastjson.JSON;
import com.empirefree.mybatisplus.entity.LogDO;
import com.empirefree.mybatisplus.mapper.LogMapper;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/01/24 16:42
 */
@Slf4j


@Aspect
@Component
public class AspectLoginLog {
    @Autowired
    private LogMapper logMapper;

    @Value("${spring.application.name}")
    private String appNname;

    @Pointcut("@annotation(com.empirefree.mybatisplus.annotation.Loginlog)")
    public void logPoint() {
    }

    @Around("logPoint()")
    public Object around(ProceedingJoinPoint joinPoint){
        Object result = null;
        LogDO logDO = new LogDO();

        try {
            RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = (HttpServletRequest) requestAttributes
                    .resolveReference(RequestAttributes.REFERENCE_REQUEST);
            MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
            UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
            //浏览器对象
            Browser browser = userAgent.getBrowser();
            //操作系统对象
            OperatingSystem operatingSystem = userAgent.getOperatingSystem();
            logDO.setBrowserName(browser.getName());
            ApiOperation aon = methodSignature.getMethod().getAnnotation(ApiOperation.class);
            if (aon != null) {
                logDO.setModuleName(aon.value());
            }
            logDO.setOsName(operatingSystem.getName());
            logDO.setIpAddr(request.getRemoteAddr());
            logDO.setAppName(appNname);
            logDO.setClassName(joinPoint.getTarget().getClass().getName());
            logDO.setMethodName(methodSignature.getMethod().getName());
            logDO.setRequestUrl(request.getRequestURI());
            logDO.setRequestMethod(request.getMethod());
            //获取请求参数
            logDO.setResultText(JSON.toJSONString(result));
            logDO.setStatus((byte) 0);
            logDO.setCreateTime(new Date());
            logDO.setCreateUserId(0L);
            logDO.setCreatePhoneNumber("1233321231");
            logDO.setCreateUserName("胡宇乔");
            long startTime = System.currentTimeMillis();
            result = joinPoint.proceed();
            long endTime = System.currentTimeMillis();
            logDO.setTakeUpTime(String.format("耗时：%s 毫秒", endTime - startTime));
            logDO.setResultText(result.toString());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            logMapper.insert(logDO);
        }
        return result;
    }
}
