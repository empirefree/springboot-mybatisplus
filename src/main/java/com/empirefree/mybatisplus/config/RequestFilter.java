package com.empirefree.mybatisplus.config;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/01/24 19:07
 */
@Slf4j
public class RequestFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        requestParams(httpServletRequest);
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
    private void requestParams(HttpServletRequest request){
        LogHttpServletRequestWrapper requestWrapper = new LogHttpServletRequestWrapper( request);
        Map<String, Object> paramsMap = getAllRequestParam(request);
        String bodyMap= requestWrapper.getBody();

        log.info("====================所有接口请求信息===========请求URI：{},请求方式;{},params请求参数:{},body请求参数:{}",request.getRequestURI(),request.getMethod(),paramsMap,bodyMap);
    }
    private Map<String, Object> getAllRequestParam(final HttpServletRequest request) {
        Map<String, Object> res = new HashMap<String, Object>();
        Enumeration<?> temp = request.getParameterNames();
        if (null != temp) {
            while (temp.hasMoreElements()) {
                String en = (String) temp.nextElement();
                String value = request.getParameter(en);
                res.put(en, value);
            }
        }
        return res;
    }
}
