package com.empirefree.mybatisplus.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.DispatcherType;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/01/24 19:25
 */
@Configuration
public class FilterConifig {
    @Bean
    public FilterRegistrationBean logFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(new RequestFilter());    //Filter实现类
        registration.addUrlPatterns("/*");              //匹配路径
        registration.setName("logFilter");              //优先级
        registration.setOrder(Integer.MAX_VALUE-1);
        return registration;
    }
}
