package com.empirefree.mybatisplus.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/02/11 15:47
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Time {
    private LocalDateTime localDateTime;
}
