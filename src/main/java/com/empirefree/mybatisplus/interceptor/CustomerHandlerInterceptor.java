package com.empirefree.mybatisplus.interceptor;

import com.empirefree.mybatisplus.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerMethodMapping;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @program: mybatisplus
 * @description:
 * @author: huyuqiao
 * @create: 2022/01/24 17:27
 */
@Component
@Slf4j
public class CustomerHandlerInterceptor implements HandlerInterceptor {

    IUserService userService;
    ApplicationContext applicationContext;
    public CustomerHandlerInterceptor(IUserService userService, ApplicationContext applicationContext){
        this.userService = userService;
        this.applicationContext = applicationContext;
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("拦截器 preHandler");
        System.out.println(userService.selectOneUser(1));
        //返回false则无法访问接口
        // 获取所有请求路径
        List<String> paths = getPath();
        log.info("CustomerHandlerInterceptor.preHandle--路径大小:{}, {}", paths.size(), request.getRequestURI().substring(5));
        // 匹配结果
        Object[] result = paths.stream().filter(path -> path.equals(request.getRequestURI().substring(5))).toArray();

        if (result.length != 0) {
            log.info("CustomerHandlerInterceptor.preHandle--正常访问");
            return true;
        } else {
            log.info("CustomerHandlerInterceptor.preHandle--进入404");
            response.sendRedirect("/test/error/404");       //注，客户端重定向redirect,forwarod请求转发是服务器内部跳转
            return false;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("拦截器 postHandler");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("拦截器 afterCompletion");
    }


    /**
     * 获取所有webe请求路径
     * @return
     */
    List<String> getPath() {
        List<String> list = new ArrayList<>();

        AbstractHandlerMethodMapping<RequestMappingInfo> objHandlerMethodMapping =
                (AbstractHandlerMethodMapping<RequestMappingInfo>) applicationContext.getBean("requestMappingHandlerMapping");

        Map<RequestMappingInfo, HandlerMethod> mapRet = objHandlerMethodMapping.getHandlerMethods();
        for (RequestMappingInfo requestMappingInfo : mapRet.keySet()) {
            Set set = requestMappingInfo.getPatternsCondition().getPatterns();
            Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                list.add(iterator.next().toString());
            }
        }
        return list;
    }
}
