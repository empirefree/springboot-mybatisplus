package com.empirefree.mybatisplus.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.empirefree.mybatisplus.annotation.Loginlog;
import com.empirefree.mybatisplus.entity.Time;
import com.empirefree.mybatisplus.entity.TimeRequest;
import com.empirefree.mybatisplus.entity.User;
import com.empirefree.mybatisplus.service.IUserService;
import com.empirefree.mybatisplus.service.TestAsync;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.assertj.core.internal.Futures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.*;

import sun.plugin2.message.Message;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.SendResult;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author empirefree
 * @since 2020-08-30
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userService;
    @Autowired
    TestAsync testAsync;

/*    private ExecutorService threadPool = new ThreadPoolExecutor(
            3,
            10,
            3,
            TimeUnit.SECONDS,
            new LinkedBlockingDeque<>(20),
            Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.DiscardOldestPolicy()
    );*/

    /**
     * @autowired修饰方法的时候会自动执行一次
     */
    @Autowired
    public void testAutowired(){
        System.out.println("asdf");
    }

    @PostMapping(value = "/testTimeTransfer")
    public Time testTimeTransfer(@RequestBody TimeRequest loginRequest){
        Time time = new Time();
        time.setLocalDateTime(loginRequest.getLocalDateTime());
        return time;
    }

    @RequestMapping(value = "/testAspect", method = RequestMethod.GET)
    public User testAspect(@RequestParam("name")String name){
        User user = new User();
        user.setName(name);
        return user;
    }
    private Integer a = new Integer(1999);

    @RequestMapping(value = "/exception",method = RequestMethod.POST)
    public Object findAll() throws Exception {
        throw new Exception("huyuqiao");
    }
    @RequestMapping(value = "/error",method = RequestMethod.POST)
    public Object error() throws Exception {
        System.out.println("UserController.error--全局异常处理");
        return "1";
    }

    /**
     * Author: HuYuQiao
     * Description:
     * 1 多个事务更新，以最后的更新为准，
     * 2 所以在对单条数据操作的时候，应该用for update 行级锁(同一事务中查后，其他事务更新、查询、删除会阻塞)
     * 3.
     *
     */
    @Transactional
    @RequestMapping(value = "/testUpdate", method = RequestMethod.GET)
    public void testUpdate() throws InterruptedException {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(User::getId, 1);
        User user = new User();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        user.setName(uuid);
        log.info("UserController.testUpdate--{}", uuid);
        userService.update(user, wrapper);
        Thread.sleep(3000);
        log.info("UserController.testUpdate--等待3秒完成");

    }
    @Transactional
    @RequestMapping(value = "/testSelectForUpdate", method = RequestMethod.GET)
    public void testSelectForUpdate() throws InterruptedException {
        userService.selectOneUserForUpdate(1);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(User::getId, 1);
        User user = new User();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        user.setName(uuid);
        log.info("UserController.testUpdate--{}", uuid);
        userService.update(user, wrapper);
        Thread.sleep(3000);
        log.info("UserController.testUpdate--等待3秒完成");

    }


    @Loginlog
    @RequestMapping(value = "/findAll",method = RequestMethod.POST)
    public Object findAll(@RequestParam(value = "huyuqiao", required = false)Integer a,  HttpServletRequest request, HttpServletResponse httpServletResponse) throws UnsupportedEncodingException {
        //获取前台发送过来的数据
        Integer pageNo = Integer.valueOf(request.getParameter("pageNo"));
        Integer pageSize = Integer.valueOf(request.getParameter("pageSize"));
        IPage<User> page = new Page<>(pageNo, pageSize);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        val user = new User();
//        user.setId(1);
        wrapper.setEntity(user);

        HttpSession session = request.getSession();
//        session.setAttribute("huyuqiao", "huyuqiao");
        System.out.println(session.getAttribute("huyuqiao") + "session值");

        new ArrayList<>(3);

        Cookie cookie = new Cookie("HappyNewYear", URLEncoder.encode(request.getSession().getId(), "utf-8"));
        cookie.setPath("/");
        cookie.setMaxAge(48 * 60 * 60);
        httpServletResponse.addCookie(cookie);

        return userService.page(page,wrapper);
    }
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public void insert() throws ExecutionException, InterruptedException {
        log.info("进入代码...");
//        threadPool.submit(() ->  {
            log.info("进入线程池" + Thread.currentThread().getName());
            Future<String> stringFuture = testAsync.asyncFunction();
//            try {
//                System.out.println("异步处理返回: " + stringFuture.get());
//            } catch (InterruptedException | ExecutionException e) {
//                e.printStackTrace();
//            }
//        });
        System.out.println("进入后续代码...");
/*        log.info("进入代码...");
        //异步回调--有返回值
        CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() ->{
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 3;
        });
        System.out.println("begin");
        future2.whenComplete((result, error) ->{
            System.out.println("返回结果:" + result);
            System.out.println("错误结果" + error);
        }).exceptionally(throwable -> {
            System.out.println(throwable.getMessage());
            return 502;
        });
        System.out.println("end");*/

    }



    @RequestMapping(value = "/testMybatisPlus", method = RequestMethod.GET)
    public List<User> testMybatisPlus(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        // 1、lambda测试
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda() .eq(User::getName, "胡宇乔")
                .or()
                .eq(User::getId, 1);
        List<User> userList = userService.list(queryWrapper);

        Cookie cookie = new Cookie("HappyNewYear", URLEncoder.encode(request.getSession().getId(), "utf-8"));
        cookie.setPath("/");
        cookie.setMaxAge(48 * 60 * 60);     //默认是关闭浏览器失效，但是如果设置了过期时间，则浏览器会缓存
        response.addCookie(cookie);
        return userList;
    }
}
