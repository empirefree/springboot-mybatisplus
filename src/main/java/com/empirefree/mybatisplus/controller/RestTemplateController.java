package com.empirefree.mybatisplus.controller;

import com.alibaba.fastjson.JSONObject;
import com.empirefree.mybatisplus.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;

/**
 * @program: mybatisplus
 * @description: 测试restTemplate
 * @author: huyuqiao
 * @create: 2021/05/20 19:00
 */
@RestController
@RequestMapping("/restTemplate")
public class RestTemplateController {

    @Autowired
    private RestTemplate restTemplate;
    /*
    * url: https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token
    * */
    @RequestMapping(value = "/getRestTemplate", method = RequestMethod.GET)
    public void getRestTemplate(){
        String url = MessageFormat.format("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_corp_tag_list?access_token={0}", "ekYD6LXA1WBl4bhVvbmNAX2ziHMfYhgxlV-0euJZTsIHW7xfA1wvU2iKoS6NO7zFjyxbuUtkhQLtHzDcnDSSr0iyDagNjKzJKV212R0r0llgUApEOZDfnzpnps_CI490SewUB2Mf6wrN896MSRyjSn4QCROTphG6vS5VsUfy3MG52VrVFGqwbpv1iCPAugmt2H1L91fsUxnnTNyNFAUcng");
        JSONObject result = restTemplate.getForEntity(url,JSONObject.class).getBody();
        System.out.println(result);

    }

    /**
     * 模拟CPU占满
     */
    @GetMapping("/cpu/loop")
    public void testCPULoop() throws InterruptedException {
        System.out.println("请求cpu死循环");
        Thread.currentThread().setName("loop-thread-cpu");
        int num = 0;
        while (true) {
            num++;
            if (num == Integer.MAX_VALUE) {
                System.out.println("reset");
            }
            num = 0;
        }

    }


}
